const shell = require('shelljs');
const config = require('./config.js');
const helpers = require('./helpers.js');
const build = helpers.build;
const copy = helpers.copy;

build('mts-sage', config.mtsSageDir, ['mvn clean install package -DskipTests=true']);

shell.echo('*************** Copying war ***************');

// Copy process
copy(`${config.mtsSageDir}/sage-war/target/sage.war`, `${config.mtsJbossDir}/server/local/deploy/deploy.last/`);