const shell = require('shelljs');
const config = require('./config.js');
const branch = config.branchToUpdate || 'dev-branch';

/** 
 * @param name {String} The project name
 * @param dir {String} The project directory
 * @param shouldStash {Boolean} OPTIONAL - Whether it should stash before merge
 */
const update = (name, dir, shouldStash = false) => {
	shell.echo(`Updating ${name}`);
	shell.echo(' ');
	shell.cd(dir);
	shouldStash === true && shell.exec('git stash');
	shell.exec(`git fetch ${config.remoteName}`);
	if(shell.exec(`git merge ${config.remoteName}/${branch}`).code !== 0){
		shell.exit(1);
	}
	shouldStash === true && shell.exec('git stash pop');
	shell.echo(' ');
	shell.echo(`Finished updating ${name}`);
};

/**
 * @param dir {String} The directory 
 * @param path {String} The file's path
 * @param isWildCard {Boolean} OPTIONAL - Whether its a wildcard
 */
const remove = (dir, path, isWildCard = false) => {
	let test = isWildCard ? shell.ls(`${dir}${path}`).length : shell.test('-e', `${dir}${path}`);
	if (test) {
		shell.rm(`${dir}${path}`);
		shell.echo(`Removed ${path}`);
	}
};

/** 
 * @param name {String} The project name 
 * @param dir {String} The project directory
 * @param commands {Array} An array of commands to execute to build
 */
const build = (name, dir, commands) => {
	shell.echo(`*************** Building ${name} ***************`);
	shell.cd(dir);

	commands.forEach(command => {
		if (shell.exec(command).code !== 0) {
			shell.echo(`*************** ${name} build failed ***************`);
			shell.exit(1);
		}
	});

	shell.echo(' ');
};

/**
 * @param filePath {String} The file path for the file you want copied
 * @param targetPath {String} The target path for the file ypu want copied over
 */
const copy = (filePath, targetPath) => {
	if (shell.test('-e', filePath)) {
		if(shell.cp(filePath, targetPath).code !== 0){
			shell.echo(`Error copying - ${filePath}`);
			shell.exit(1);			
		}
	} else {
		shell.echo(`Error cp - ${filePath} does not exist`);
		shell.exit(1);
	}
};

module.exports = { update, remove, build, copy };
