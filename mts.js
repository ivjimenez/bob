const shell = require('shelljs');
const config = require('./config.js');
const helpers = require('./helpers');

const update = helpers.update;
const remove = helpers.remove;
const build = helpers.build;
const copy = helpers.copy;

if (process.env.UPDATE === 'true') {
	shell.echo('*************** Updating Projects ***************');
	// Catman
	update('Catman', config.mtsCatmanDir);

	// Tribe
	update('Tribe', config.mtsTribeDir);

	// MTS Data Service
	update('MTS Data Service', config.mtsCmsDataServiceDir);
}

shell.echo('*************** Removing old .wars / .ears ***************');

remove(config.mtsSageDir, '/sage-war/target/sage.war');
remove(config.mtsCmsDataServiceDir, '/mts-cms-data-service-ear/target/mts-cms-data-service.ear');
remove(config.mtsTribeDir, '/tribe-handyman/target/tribe-handyman-*.war', true);
remove(config.mtsCatmanDir, '/dist/cegCategories.war');
remove(config.mtsJbossDir, '/server/local/deploy/deploy.last/sage.war');
remove(config.mtsJbossDir, '/server/local/deploy/mts-cms-data-service.ear');
remove(config.mtsJbossDir, '/server/local/deploy/deploy.last/tribe-handyman-*.war', true);
remove(config.mtsJbossDir, '/server/local/deploy/deploy.last/cegCategories.war');

shell.echo('Finished : Removing old .wars / .ears');

shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo(' ');
shell.echo('Hopefully you manually updated the mts-sage repo');
shell.echo(' ');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo(' ');

// Begin Building
build('mts-sage', config.mtsSageDir, ['mvn clean install package -DskipTests=true']);
build('mts-catman', config.mtsCatmanDir, ['ant -buildfile build.xml clean', 'ant -buildfile build.xml compile', 'ant -buildfile build.xml war']);
build('mts-tribe', config.mtsTribeDir, ['mvn clean install package -DskipTests=true']);
build('mts-cms-data-service', config.mtsCmsDataServiceDir, ['mvn clean package -Dmaven.test.skip=true']);

shell.echo('*************** Copying Files ***************');

// Copy process
copy(`${config.mtsSageDir}/sage-war/target/sage.war`, `${config.mtsJbossDir}/server/local/deploy/deploy.last/`);
copy(`${config.mtsCmsDataServiceDir}/mts-cms-data-service-ear/target/mts-cms-data-service.ear`, `${config.mtsJbossDir}/server/local/deploy`);
shell.cp(`${config.mtsTribeDir}/tribe-handyman/target/[tribe-handyman-]\*.war`, `${config.mtsJbossDir}/server/local/deploy/deploy.last/`);
copy(`${config.mtsCatmanDir}/dist/cegCategories.war`, `${config.mtsJbossDir}/server/local/deploy/deploy.last/`);
shell.echo('Finished : Copying Files');
