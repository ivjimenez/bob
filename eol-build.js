const shell = require('shelljs');
const config = require('./config.js');
const helpers = require('./helpers');

const update = helpers.update;
const build = helpers.build;

if (process.env.UPDATE === 'true') {
	update('eonline-service', config.serviceDir);
	update('eonline-commons-client', config.cciDir, true);
}
else {
	shell.echo('Skipping - updating projects');
}

build('eonline-commons-client', config.cciDir, ['mvn -DskipTests clean remote-resources:bundle install']);
build('eonline-service', config.serviceDir, ['mvn -DskipTests clean remote-resources:bundle install']);