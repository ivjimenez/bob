module.exports = {
	// To build Sage 
	mtsSageDir : '~/Documents/eol/workspace/mts-sage',
	mtsCatmanDir : '~/Documents/eol/workspace/mts-catman',
	mtsTribeDir : '~/Documents/eol/workspace/mts-tribe', 
	mtsCmsDataServiceDir : '~/Documents/eol/workspace/mts-cms-data-service',
	mtsJbossDir : '~/Documents/eol/eonline-mobile-servers/mts-sage-jboss',
	// To build CCI & Service for EOL Projects (NOT NEEDED TO BUILD SAGE)
	serviceDir : '~/Documents/eol/workspace/eonline-service',
	cciDir : '~/Documents/eol/workspace/eonline-commons-client',
	// Misc Options
	branchToUpdate : 'dev-branch',
	remoteName : 'upstream'
};